import './ReactotronConfig'
import React, { Component } from 'react'
import {View } from 'react-native'
import {Root,StyleProvider,Spinner} from 'native-base'
import getTheme from './theme/components'
import theme from './theme/variables'
import platform from './theme/variables/platform'
import material from './theme/variables/material'
import AppNavigator from './AppNavigator'

import configureStore from './store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
// const store = configureStore()
const { store, persistor } = configureStore()

import Reactotron from 'reactotron-react-native'

export default class App extends React.Component {
  state = {
    isReady: false,
  }

 
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'Ionicons': require('native-base/Fonts/Ionicons.ttf'),
      // 'Lato' : require('./theme/fonts/Lato/Lato-Regular.ttf')
      'SourceSansPro' : require('./theme/fonts/Source_Sans_Pro/SourceSansPro-Regular.ttf'),
      'SourceSansPro_bold' : require('./theme/fonts/Source_Sans_Pro/SourceSansPro-Bold.ttf'),
      'SourceSansPro_italic' : require('./theme/fonts/Source_Sans_Pro/SourceSansPro-Italic.ttf')
      // 'Roboto' : require('./theme/fonts/Roboto/Roboto-Medium.ttf')
    });
    this.setState({isReady: true})
  } 
 
  render() {
    /*
    dispatch,getState,subscribe
    console.log(store) 
    console.log(store.getState())
    */

    /*
    store.subscribe(()=>{
      console.log("=== store change === :", store.getState())
    })
    store.dispatch({type:"ADD_CART",payload:{productName:'My Product 2',Qty:1}})
    store.dispatch({type:"ADD_CART",payload:{productName:'My Product 3',Qty:3}})
    store.dispatch({type:"ADD_CART",payload:{productName:'My Product 4',Qty:2}})
    */



    if (!this.state.isReady) {
      return <Expo.AppLoading />
    }

    return (
      <StyleProvider style={getTheme(theme)}>
        <Provider store={store}>
          {/* <PersistGate persistor={persistor}> */}
          <Root> 
            <AppNavigator store={store} onNavigationStateChange={null} />
          </Root>
          {/* </PersistGate> */}
        </Provider>
      </StyleProvider>  
    );
  }
}


