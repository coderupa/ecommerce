import React, { Component } from 'react'
import {Platform,View} from 'react-native'
import {StackNavigator,DrawerNavigator} from 'react-navigation'
import Home from './screens/Home'
import ProductList from './screens/ProductList'
import ProductDetail from './screens/ProductDetail'
import Cart from './screens/Cart'
import LoginRegister from './screens/LoginRegister'
import Account from './screens/Account'
import EditProfile from './screens/EditProfile'
import ChangePassword from './screens/ChangePassword'
import OrderHistory from './screens/OrderHistory'
import Checkout from './screens/Checkout'
import Payment from './screens/Payment'
import Shipping from './screens/Shipping'
import OrderSuccess from './screens/OrderSuccess'
import SimpleForm from './screens/SimpleForm'
import TestScreen from './screens/test'
import {DrawerMenu} from './components/'
import { connect} from 'react-redux';

// let isLogin=false;

// const checkAuth=(route)=>{
//   console.log(isLogin,route)
//   if(isLogin==true){
//     return route 
//   }else{
//     return LoginRegister 
//   }
// }

const routerConfig ={
  Home: { screen: Home},
  ProductList: { screen: ProductList},
  ProductDetail: { screen: ProductDetail},
  Cart: { screen: Cart},
  LoginRegister:{screen:LoginRegister},
  Account:{screen:Account},
  EditProfile:{screen:EditProfile},
  ChangePassword:{screen:ChangePassword},
  OrderHistory:{screen:OrderHistory},
  Checkout:{screen:Checkout},
  Payment:{screen:Payment},
  Shipping:{screen:Shipping},
  OrderSuccess:{screen:OrderSuccess},
  SimpleForm:{screen:SimpleForm},
  TestScreen:{screen:TestScreen},
}
 

const Drawer = DrawerNavigator(
  routerConfig,
  {
    initialRouteName: "Home",
    contentComponent: DrawerMenu,
    contentOptions: {
      activeTintColor: '#e91e63',
      style: {
        flex: 1,
        paddingTop: 15,
      }
    }
  }
)


const render = (user)=>{
  //  isLogin=user.isSuccess
   console.log('render')
   console.log(user)
  //  return(
  //   <Drawer/>
  //  )

}

const AppNavigator = () => {
  // console.log('=====AppNavigator========')
  // console.log(store)
  // isLogin=store.getState().user.isSuccess
  // console.log(isLogin)
  // store.subscribe(render)
  // store.subscribe(render)
  // isLogin = props.store.getState().user.isSuccess
  // console.log('render appnavigator')
  return <Drawer/>
}

class AppNavigatorx extends Component{
  state={
    isLogin:false
  }
  
  _renderD(){
    return(
      DrawerNavigator(
        {
          Home: { screen: Home},
          ProductList: { screen: (ProductList)},
          ProductDetail: { screen: ProductDetail},
          Cart: { screen: Cart},
          LoginRegister:{screen:LoginRegister},
          Account:{screen:Account},
          EditProfile:{screen:EditProfile},
          ChangePassword:{screen:ChangePassword},
          OrderHistory:{screen:OrderHistory},
          Checkout:{screen:Checkout},
          Payment:{screen:Payment},
          Shipping:{screen:Shipping},
          OrderSuccess:{screen:OrderSuccess},
          SimpleForm:{screen:SimpleForm},
          TestScreen:{screen:TestScreen},
        },
        {
          initialRouteName: "Home",
          contentComponent: DrawerMenu,
          contentOptions: {
            activeTintColor: '#e91e63',
            style: {
              flex: 1,
              paddingTop: 15,
            }
          }
        }
      )
    )
  }
  checkAuth=(route)=>{
    if(isLogin==true){
      return route 
    }else{
      return LoginRegister 
    }
  }

  render(){
    isLogin=this.props.store.getState().user.isSuccess
    // console.log(isLogin)
    return <Drawer/>
    // return this._renderD()
  }
}
export default AppNavigator;

// const mapStateToProps = state => ({
//   user: state.user,
// })
// const mapDispatchToProps = {
// }
// export default connect(mapStateToProps,mapDispatchToProps)(AppNavigator)
 