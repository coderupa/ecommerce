import Reactotron, { trackGlobalErrors,networking,openInEditor,overlay } from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'
Reactotron
  .configure() // controls connection & communication settings
  .use(reactotronRedux())
  .use(trackGlobalErrors())
  // .use(openInEditor())
  // .use(overlay())
  .use(networking())
  .useReactNative() // add all built-in react native plugins
  .connect() // let's connect!