// const endpoint='http://192.168.1.102:3000/products'
// const endpoint='http://localhost:3000/products'
import Hashes from 'jshashes'
const endpoint='https://api.marketcloud.it/v0/'
const publicKey='405bfee4-79a2-4845-9519-7f06693e0c52'
const secretKey='3ZklE7etK3sa06u7d7VIwiVOOxXw0TSohmFYgXfJ2Ok-'

class API{

  constructor (publicKey,secretKey) {
    this.publicKey=publicKey
    this.secretKey=secretKey
  }

  appInfo = async(token) =>{
    let url=`${endpoint}application`
    if (token ==""){
      token = this.publicKey
    }else{
      token = this.publicKey +':'+ token
    }
    try {
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':token 
        }
      })
      response=await response.json()
      return response  
    } catch (error) {
      return error 
    }
  }

  getToken=async ()=>{
    let currentTime = Date.now()
    let hash_secretKey = new Hashes.SHA256().b64(this.secretKey+currentTime)
    let url=`${endpoint}tokens`
    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': this.publicKey 
        },
        body:JSON.stringify({
          "publicKey" :this.publicKey,
          "secretKey" :hash_secretKey,
          "timestamp" :currentTime,
          })
        }
      )
      return await response.json()        
    } catch (error) {
      console.log(error)
    }
  }

  readProducts = async (page=1)=>{
    let url=`${endpoint}products?page=${page}`
    try {
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey 
        }
      });
     
      // let count=response.headers.get('x-total-count')
      // let result={
      //   "data" : await response.json() ,
      //   "count" :count
      // }
      return await response.json()  
    } catch (error) {
      console.log('error found ' + error)
    }
  }

  login=async(email,password)=>{
    let url=`${endpoint}users/authenticate`
    let response = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': this.publicKey
      },
      body:JSON.stringify({
         email,
         password 
        })
      }
    )
    return await response.json()
  }

  register=async(email,name,password,image_url="",token)=>{
    let url=`${endpoint}users`
    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': this.publicKey + ':' + token
        },
        body:JSON.stringify({
          email,
          password,
          name,
          image_url 
          })
        }
      )
      return await response.json()       
    } catch (error) {
      console.log(error)
    }
  }

  changePassword=async(old_password,new_password,userId,userToken)=>{
    let url=`${endpoint}users/${userId}/updatePassword`
    try {
      let response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        },
        body:JSON.stringify({
          old_password,
          new_password,
          })
      })
      return response.json()
    } catch (error) {
      console.log('error found ' + error)
    }    
  }

  editProfile=async(name,email,userId,userToken)=>{
    let url=`${endpoint}users/${userId}`
    try {
      let response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        },
        body:JSON.stringify({
          name,
          email,
          })
      })
      return response.json()
    } catch (error) {
      console.log('error found ' + error)
    }    
  }

  orderHistory=async(userToken)=>{
    let url=`${endpoint}orders`
    try {
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        }
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }

  shippingMethods=async()=>{
    let url=`${endpoint}shippings`
    try {
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':this.publicKey 
        }
      })
      return response.json()
    } catch (error) {
      return error
      // console.log('error found ' + error)
    } 
  }

  paymentMethods=async()=>{
    let url=`${endpoint}paymentmethods`
    try {
      let response = await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':this.publicKey 
        }
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }

  createOrder=async(items,shipping_id,shipping_address,billing_address,userToken)=>{
    // console.log(items,shipping_id,shipping_address,billing_address,userToken)
    let url=`${endpoint}orders`
    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        },
        body:JSON.stringify({
          cart_id:0,
          shipping_address_id:0,
          items,
          shipping_address,
          billing_address
        })
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }


  createPayment=async(payment_method_id,order_id,userToken)=>{
    let url=`${endpoint}payments`
    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        },
        body:JSON.stringify({
          method:"Custom",
          payment_method_id,
          order_id
        })
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }

  createPaymentStripe=async(order_id,cardInfo,userToken)=>{
    // console.log(order_id,cardInfo,userToken)
    let url=`${endpoint}payments`
    let stripe_token=await this.stripeToken(cardInfo)
    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization':publicKey +':'+ userToken 
        },
        body:JSON.stringify({
          "method":"Stripe",
          "order_id":order_id,
          "source":stripe_token.id
        })
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }
  
  stripeToken=async(cardInfo)=>{
    let url='https://api.stripe.com/v1/tokens'
    let stripe_api_key="sk_test_UkiKceuLTApjBEJ9J6Nq8Lj6"
    
    let expiry=cardInfo.expiry.split('/')
    let cardDetails={
      "card[number]":cardInfo.number,
      "card[exp_month]":expiry[0],
      "card[exp_year]":expiry[1],
      "card[cvc]":cardInfo.cvc
    }

    //encode cardDetail
    let formBody = [];
    for (let property in cardDetails) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(cardDetails[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    

    try {
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization':'Bearer ' + stripe_api_key 
        },
        body:formBody
      })
      return response.json()
    } catch (error) {
      return error
    } 
  }

 


}

// export default new API(publicKey,secretKey)
export default new API(publicKey,secretKey)