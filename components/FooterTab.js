import React, { Component } from 'react'
import { Text as MyText,View } from 'react-native'
import {
  Footer,
  FooterTab,
  Button,
  Text,
  Icon,
  Badge
} from 'native-base'

import {IconBadge} from '../components'

export default ({navigate,cartSum}) => (
  <FooterTab>
    <IconBadge 
      name="ios-home" 
      text="Home" 
      onPress={()=>navigate('Home')} 
    /> 
    <IconBadge 
      name="ios-pricetags" 
      text="Catalog" 
      onPress={()=>navigate('ProductList')} 
    /> 
    <IconBadge 
      name="ios-cart" 
      number={cartSum} 
      text="Cart" 
      onPress={()=>navigate('Cart')} 
    /> 
    <IconBadge 
      name="ios-contact" 
      text="My Account" 
      onPress={()=>navigate('Account')} 
    /> 
  </FooterTab>
)
