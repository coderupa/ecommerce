import React, { Component } from 'react'
import { View,Text,StyleSheet } from 'react-native'
import { Button,Icon } from 'native-base'


export default ({name,number,text,onPress})=>(
  <Button transparent onPress={onPress}>
    { number > 0 ?
    <View style={styles.cartWrapper}>
      <Text style={styles.cartTitle}>{number}</Text>
    </View>
   :null  }
    <Icon name={name}/>
    <Text bold>{text}</Text>
  </Button>
)


const styles=StyleSheet.create({
  cartWrapper:{
    position:'absolute',
    zIndex:10,
    padding:0,
    alignSelf:'flex-end',
    top:5,
    right:6,
    justifyContent:'center',
    alignItems:'center',
    width:20,
    height:20,
    borderRadius:10,
    backgroundColor:'lightcoral'
  },
  cartTitle:{
    fontSize:9,
    fontWeight:'bold',
    color:"#FFF",
    padding:0
  }
})