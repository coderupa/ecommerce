import React, { Component } from 'react'
import { View,TextInput,StyleSheet,TouchableOpacity,Text } from 'react-native'
import PropTypes from 'prop-types'
export default class QuantityInput extends Component {
  state = { 
    value:this.props.value,
    buttonWidth:null,
    inputWidth:null
  }
  _updateLayout=(event)=>{
    let {width,height} = event.nativeEvent.layout
    this.setState({
      buttonWidth:0.3 * width,
      inputWidth:0.4 * width
    })
  }

  _minusButton=()=>{
    let value=this.state.value
    if (value > this.props.minValue) {
     this.setState({value:value-1})
     this.props.onChange(value-1)
    }
  }
  
  _plusButton=()=>{
    let value=this.state.value
    this.setState({value:value+1})
    this.props.onChange(value+1)
  }

  render() {
    const{width,value} = this.props
    return (
      <View style={styles.wrapper} onLayout={event => this._updateLayout(event)}>
        <TouchableOpacity 
          style={[styles.button,{width:this.state.buttonWidth,borderRightWidth:1,borderRightColor:"#F1F1F1"}]} 
          onPress={()=>this._minusButton()}>
          <Text style={styles.buttonCaption}>-</Text>
        </TouchableOpacity>
        <View style={styles.inputWrapper}>
          <Text style={[styles.input,{width:this.state.inputWidth}]}>{this.state.value}</Text> 
        </View>
        <TouchableOpacity 
          style={[styles.button,{width:this.state.buttonWidth,borderLeftWidth:1,borderLeftColor:"#F1F1F1"}]}
          onPress={()=>this._plusButton()}
        >
          <Text style={styles.buttonCaption}>+</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

QuantityInput.propTypes={
 width:PropTypes.number,
 value:PropTypes.number,
 maxValue:PropTypes.number,
 minValue:PropTypes.number,
}

QuantityInput.defaultProps={
  width:null,
  value:1,
  minValue:0,
  onChange:()=>null
}

const styles=StyleSheet.create({
  wrapper:{
    flex:1,
    flexDirection:'row',
    backgroundColor:"#FFF",
    borderWidth:1,
    borderColor:'#777'
  },
  inputWrapper:{
    alignItems:'center',
    justifyContent:'center'
  },
  input:{
    textAlign:'center',
    paddingVertical:7,
    fontSize:17
  },
  button:{
    paddingHorizontal:2,
    paddingVertical:5,
    backgroundColor:'#777',
    alignItems:'center',
    justifyContent:'center'
  },
  buttonCaption:{
    color:"#FFF",
    fontSize:17
  }


})