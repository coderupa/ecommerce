import React,{Component} from 'react'
import { View } from 'react-native';
import {Icon,Text,Form,Item,Label,Input,InputGroup} from 'native-base'
// import DatePicker from '../../components/date-picker'
// import SegmentedControlTab from 'react-native-segmented-control-tab'
// import * as Animatable from 'react-native-animatable';
// import {formatDate} from '../../utilities'

const FieldLabel= (props) => {
  if (props.success){
    props.style.color='green' 
  }

  if (props.error){
    props.style.color='red' 
  }
  
  return <Label  {...props}>{props.children}</Label>
}

class RenderField extends Component {
  state = {}

  componentDidMount() {
    const { input: { value, onChange },options,type} = this.props
    if (type=='segmented') {
      defaultIndex=this.props.defaultIndex
      if (this.props.input.value == "") {
        this.props.input.onChange(defaultIndex)
        this.setState({selectedIndex:defaultIndex})
      }else{
        this.setState({selectedIndex:value})
      }
    }
  }

  render() {
    const { input, label, type, placeholder,icon,autoFocus,onMyFocus,
      returnKeyType="next",keyboardType="default",load,autoCapitalize,secureTextEntry,
      editable=true,
      meta: { touched, error, warning,active,visited } } = this.props
      let focus;
      var hasError= false;
      touched == true ? focus=true : focus=false
      if(error !== undefined && visited == true && active == false){
        hasError= true;
        focus=false;
      }
     
    return (
      <Item stackedLabel success={active} error={hasError}>
        <FieldLabel success={active} error={hasError} style={{fontWeight:"700"}}>{placeholder} : </FieldLabel>
        {/* {hasError == true ? <Label style={{color:"red"}}><Animatable.Text animation="fadeIn">Error Message</Animatable.Text></Label> : null}  */}
        <Item>
          <Input {...input} 
          keyboardType={keyboardType} returnKeyType={returnKeyType} 
          autoCapitalize={autoCapitalize} secureTextEntry={secureTextEntry}  
          autoFocus={autoFocus}  editable ={editable} onChangeText={value=>input.onChange('',value)}
          />
        </Item>
      </Item>
     
      // <View>
      //   <InputGroup  style={{marginTop:10}} success={active} error={hasError}>
      //     <Icon name={icon} style={{color:'#CCC'}}/>
      //   </InputGroup>
      //   {hasError ? <Text>Hello {error}</Text> : <Text />}    
      // </View>
    )
    }
}

export default RenderField; 