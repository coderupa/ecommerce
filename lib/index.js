import Intl from 'intl'
// import 'intl/locale-data/jsonp/en.js';
import 'intl/locale-data/jsonp/id.js';
import Reactotron from 'reactotron-react-native'
import Moment from 'moment' 

// reference : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
export const formatMoney =(number) => {
  formatter = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 0, /* this might not be necessary */
  })
  return (formatter.format(number).replace(/^(\D+)/, '$1 '))
}

export const formatDate=(myDate,dateFormat='dddd, DD MMMM YYYY') => {
  Moment.locale('id')
  return (
    Moment(myDate).format(dateFormat) 
  )
}

export const cartSum=cart => ( cart.reduce((acc, cur) => acc + cur.Qty,0))