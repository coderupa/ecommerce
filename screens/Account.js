import React, { Component } from 'react'
import { View, Text as MyText,TouchableOpacity,StyleSheet } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Input,
  Item,
  Label,
  Form
 } from "native-base"
 import {formatDate,cartSum} from '../lib'
 import {FooterTab,IconBadge} from '../components'

 import { connect} from 'react-redux'
 import LoginRegister from './LoginRegister'
 import * as userActions from '../store/user'

class Account extends Component {
  state = {
    name:this.props.user.data.name,
    email:this.props.user.data.email
  }


  componentDidMount () {
    // this.setState({
    //   name:this.props.user.data.name
    // })
  }
  
  render() {
    const {navigate} = this.props.navigation
    const {isSuccess,data} = this.props.user
    let image_url = data.image_url !== '' ? {uri:data.image_url}:{}
    let defaultAvatar=require('../assets/avatar.png')

    if (!this.props.user.isSuccess){
      return <LoginRegister navigation={this.props.navigation}/>
    }

    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ () => navigate('Home')}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>My Account</Title>
        </Body>
        <Right>
          <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/>
        </Right>
        </Header>
        <Content padder>
          <View style={styles.profileBox}> 
            <Thumbnail large circle source={image_url} defaultSource={defaultAvatar} style={{alignSelf:'center'}}/>
            <Text style={styles.profileName}>{data.name}</Text>
            <Text style={styles.profileEmail}>{data.email}</Text>
            <Text note style={{textAlign:'center'}}> Join Since : {formatDate(data.created_at)}</Text>
         </View>
         <View style={styles.actions}>
            <Button full iconLeft bordered danger style={styles.actionButton} onPress={()=>navigate('EditProfile')}>
             <Icon name='ios-contact-outline' />
             <Text>Edit Profile</Text>
           </Button>

            <Button full iconLeft bordered success style={styles.actionButton} onPress={()=>navigate('ChangePassword')}>
             <Icon name='ios-lock-outline' />
             <Text>Change Password</Text>
           </Button>
           
           <Button full iconLeft bordered style={styles.actionButton} onPress={()=>navigate('OrderHistory')}>
             <Icon name='ios-browsers-outline' />
             <Text>Order History (10) </Text>
           </Button>
           
           <Button full iconLeft bordered warning 
              style={styles.actionButton}
              onPress={()=>this.props.logout()}
            >
             <Icon name='ios-unlock-outline' />
             <Text>Logout </Text>
           </Button>
         </View>
        </Content>
        <Footer>
          <FooterTab  navigate={navigate} cartSum={cartSum(this.props.cart)}/>
        </Footer>
        </Container>
    )
  }
}
const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user
})
const mapDispatchToProps = {
  logout:userActions.reset
}

export default connect(mapStateToProps,mapDispatchToProps)(Account)

const styles=StyleSheet.create({
  profileBox:{
    alignItems:'center',
    marginVertical:20
  },
  profileName:{
    marginTop:5,
    fontSize:20,
    fontWeight:'bold'
  },
  profileEmail:{

  },
  actions:{
    padding:50
  },
  actionButton:{
    marginVertical:5
  }
})