import React, { Component } from 'react'
import { View,TouchableOpacity,Alert } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  List,
  ListItem,
  Thumbnail,
  Grid,
  Col,
  Footer
 } from "native-base"
 import { connect} from 'react-redux';
 import * as cartActions from '../store/cart';
 import {FooterTab,IconBadge,QuantityInput} from '../components'
 import {formatMoney} from '../lib'
 const productPlaceholder= require('../assets/product-placeholder.jpg')
 

const CartItem=({id,image,productName,price,Qty,updateCart,removeCart})=>(
  <ListItem>
    <Thumbnail 
      square size={100} 
      source={{ uri: image }}  
      defaultSource={productPlaceholder} />
    <Body>
      <Text style={{paddingVertical:5,fontWeight:"700"}}>{productName}</Text>
      <Grid>
        <Col size={4} style={{justifyContent:'center'}}>
          <Text note>{formatMoney(price)} </Text>
        </Col>
        <Col size={3}>
          <QuantityInput value={Qty} onChange={(Qty)=>updateCart({id,Qty},true)} />
        </Col>
        <Col size={2}>
          <Button style={{alignSelf:"flex-end"}} transparent danger onPress={()=>removeCart({id})}>
            <Icon name="md-trash"/>
          </Button>
        </Col>
      </Grid>
    </Body>
  </ListItem>
) 

class Cart extends Component {
  state = {  }

  componentDidMount () {
    // this.props.updateCart({ id: 3, productName: "Product 3","price":350.000,Qty:1})
  }

  _getTotal(){
    return this.props.cart.reduce((acc, cur) => acc + cur.Qty * cur.price,0);
  }
  
  render() {
    const {navigate,goBack} = this.props.navigation
    const {isSuccess} = this.props.user
    const nextScreen = isSuccess == '' ? 'Account':'Checkout'
    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ () => goBack()}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Cart</Title>
        </Body>
        <Right/>
        </Header>
        <Content>
        <List>
          {
            this.props.cart.map((cart => 
              <CartItem 
                key={cart.id} {...cart} 
                updateCart={this.props.updateCart} 
                removeCart={this.props.removeCart}
              />))
          }
        </List>
        </Content>
        <Footer style={{paddingBottom:0}}>
        <Left>
          <Text style={{alignSelf:'center'}}>Total: </Text>
          <Text style={{alignSelf:'center',fontWeight:'bold',fontSize:15}}>{formatMoney(this._getTotal())} </Text>
        </Left>
        <Body>
          <Button full iconRight onPress={()=>navigate(nextScreen,{from:'Cart'})} >
            <Text>Checkout</Text>
            <Icon style={{color:"#FFF"}} name="md-arrow-forward"/>
          </Button>
        </Body>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  cart: state.cart,
  user: state.user
})
const mapDispatchToProps = {
  updateCart:cartActions.update,
  removeCart:cartActions.remove
}
// export default connect(mapStateToProps,mapDispatchToProps)(Cart)
export default connect(mapStateToProps,mapDispatchToProps)(Cart)