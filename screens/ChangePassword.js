import React, { Component } from 'react'
import { View, Text as MyText,TouchableOpacity,StyleSheet } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Toast,
  Spinner
 } from "native-base"
 import { connect} from 'react-redux'
 import {FooterTab,IconBadge} from '../components'
 import * as userAction from '../store/user'
 import API from '../api'
 import {cartSum} from '../lib'
 const Profile =() => <Text>Profile</Text>
 const Orders =() => <Text>Orders</Text>

class ChangePassword extends Component {
  state = {
    securePassword : true,
    password1:'',
    password2:'',
    isRequest:false,
  }


  changePasswordSecure(){
    securePassword=this.state.securePassword
    this.setState({securePassword : !securePassword})
  }

 
  _update=async(navigate)=>{
    let {password1,password2}=this.state 
    if (
      password1 !== password2 ||
      password1 =='' ||
      password2 ==''
    ){
      Toast.show({
        text:'Enter your password correctly',
        position:'bottom',
        buttonText:'OK',
        type:'danger',
        duration:5000,
      })
    }else{
      await this.setState({isRequest:true})
      let result= await API.changePassword(password1,password2,this.props.user.data.id,this.props.user.token)
      if (result.status==true){
        Toast.show({
          text:'Password change succesfully',
          position:'bottom',
          buttonText:'OK',
          type:'success',
          duration:5000,
          onClose:()=>{this.props.logout();navigate('Account')}
        })
      }else{
        Toast.show({
          text:result.errors[0].message,
          position:'bottom',
          buttonText:'OK',
          type:'danger',
          duration:5000,
          onClose:this.setState({isRequest:false})
        })      
      }
    }
  }

  render() {
    const {navigate} = this.props.navigation
    const {data} = this.props.user
    const {isRequest} = this.state
    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ () => navigate('Account')}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Edit Profile</Title>
        </Body>
        <Right>
        <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/>
        </Right>
        </Header>
        <Content padder>
        <Form>
        <Item >
          <Input 
            placeholder="Current Password" 
            value={this.state.password1} 
            editable={!isRequest}
            secureTextEntry={this.state.securePassword}
            onChangeText={(value)=>this.setState({password1:value})}  />
          <Button dark transparent onPress={()=> this.changePasswordSecure()}>
            <Icon active name={this.state.securePassword ?'ios-eye':'ios-eye-off'} />
          </Button>
        </Item>
        <Item>
          <Input 
            placeholder="New Password" 
            value={this.state.password2} 
            editable={!isRequest}
            secureTextEntry={this.state.securePassword}
            onChangeText={(value)=>this.setState({password2:value})}  />
          <Button dark transparent onPress={()=> this.changePasswordSecure()}>
            <Icon active name={this.state.securePassword ?'ios-eye':'ios-eye-off'} />
          </Button>
        </Item>
        
      </Form>
          <Button block style={{margin:15,marginTop:40}} onPress={()=>this._update(navigate)}>
            <Text>Update</Text>
            { isRequest == true ?  <Spinner color="white"/> : null }
          </Button>
        </Content>
        <Footer>
          <FooterTab  navigate={navigate}/>
        </Footer>
        </Container>
    )
  }
}
const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user
})
const mapDispatchToProps = {
  logout:userAction.reset
}

export default connect(mapStateToProps,mapDispatchToProps)(ChangePassword)

const styles=StyleSheet.create({
  profileBox:{
    alignItems:'center',
    marginVertical:20
  },
  profileName:{
    marginTop:5,
    fontSize:20,
    fontWeight:'bold'
  },
  profileEmail:{

  },
  actions:{
    padding:50
  },
  actionButton:{
    marginVertical:5
  }
})