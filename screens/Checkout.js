import React, { Component } from 'react'
import { View, Text as MyText,TouchableOpacity,StyleSheet } from 'react-native'
import Shipping from './Shipping' 
import Payment from './Payment' 
import OrderSuccess from './OrderSuccess' 
import {cartSum} from '../lib'
 
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Spinner,
  Toast
 } from "native-base"
 import { connect} from 'react-redux'
 import {FooterTab,IconBadge,CheckoutStep} from '../components'
 import API from '../api'

class Checkout extends Component {
  state = {
    currentPosition:0,
    shippingMethods:[],
    paymentMethods:[],
    order_id:'',
    isRequest:false,
    isFailure:false
  }

  _prepareData=async()=>{
    await this.setState({isRequest:true})

    // get payment options
    let paymentMethods=await API.paymentMethods()
    if(paymentMethods.status ==true){
      await this.setState({ paymentMethods:paymentMethods.data})
    }
    
    // get shipping options
    let shippingMethods=await API.shippingMethods()
    if(shippingMethods.status ==true){
      await this.setState({ shippingMethods:shippingMethods.data})
    }

    await this.setState({isRequest:false})
     
    if (paymentMethods.status==false || shippingMethods.status == false){
      Toast.show({
        text:'Checkout Failed',
        position:'bottom',
        buttonText:'OK',
        type:'danger',
        duration:5000,
      })
      await this.setState({isFailure:true})
    }
    // let shippingMethod
  }

  _renderSteps=()=>{
    switch (this.state.currentPosition) {
      case 0:
        return <Shipping actionStep={this._shippingAction} shippingMethods={this.state.shippingMethods}/>
      case 1:
        return <Payment actionStep={this._paymentAction} paymentMethods={this.state.shippingMethods} navigation={this.props.navigation}/>
      case 2:
        return <OrderSuccess actionStep={this._actionStep} navigation={this.props.navigation}/>
    }
  }

  _shippingAction=async(address)=>{
    await this.setState({ currentPosition:1 })
  }

  _paymentAction= async ()=>{
    await this.setState({ currentPosition:2 })
  }

  _actionStep=(key)=>{
    // let current=this.state.currentPosition
    // current = key=='next' ? current +1 : current -1
    // this.setState({
    //   currentPosition:current
    // })
  }

  componentDidMount () {
    this._prepareData()
  }
  
  _navigateStep=()=>{
    if (this.state.currentPosition==0){
      this.props.navigation.goBack(null)
    }else{
      this.setState({currentPosition:this.state.currentPosition-1})
    }
  }

  render() {
    const {navigate,goBack} = this.props.navigation
    const {currentPosition,shippingMethods,paymentMethods,isRequest} = this.state
    const {isSuccess,data} = this.props.user
    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ ()=>this._navigateStep()}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Checkout</Title>
        </Body>
        <Right>
        <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/>
        </Right>
        </Header>
        <View style={{flex:1}}>
          <CheckoutStep currentPosition={currentPosition}/>
          {isRequest==true?<Spinner/>:this._renderSteps()}
        </View>
        
        </Container>
    )
  }
}
const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user,
  
})
const mapDispatchToProps = {
}

export default connect(mapStateToProps,mapDispatchToProps)(Checkout)

const styles=StyleSheet.create({
  
})