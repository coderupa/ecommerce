import React, { Component } from 'react'
import { View, Text as MyText,TouchableOpacity,StyleSheet } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Toast,
  Spinner
 } from "native-base"
 import { connect} from 'react-redux'
 import {FooterTab,IconBadge} from '../components'
 import * as userAction from '../store/user'
 import API from '../api'
 import {cartSum} from '../lib'

 const Profile =() => <Text>Profile</Text>
 const Orders =() => <Text>Orders</Text>

class EditProfile extends Component {
  state = {
    securePassword : true,
    email:this.props.user.data.email,
    name:this.props.user.data.name,
    isRequest:false,
  }

  _getTotalQty(){
    return this.props.cart.reduce((acc, cur) => acc + cur.Qty,0);
  }

  changePasswordSecure(){
    securePassword=this.state.securePassword
    this.setState({securePassword : !securePassword})
  }

  _update=async(navigate)=>{
    let {name,email}=this.state 
    const {data:{id},token} =this.props.user
    if (
      name =='' ||
      email ==''
    ){
      Toast.show({
        text:'Enter form correctly',
        position:'bottom',
        buttonText:'OK',
        type:'danger',
        duration:5000,
      })
    }else{
      await this.setState({isRequest:true})
      let result= await API.editProfile(name,email,id,token)
      if (result.status==true){
        Toast.show({
          text:'Profile updated',
          position:'bottom',
          buttonText:'OK',
          type:'success',
          duration:5000,
          onClose:()=>{this.props.logout();navigate('Account')}
        })
      }else{
        Toast.show({
          text:result.errors[0].message,
          position:'bottom',
          buttonText:'OK',
          type:'danger',
          duration:5000,
          onClose:this.setState({isRequest:false})
        })      
      }
    }
  }

  render() {
    const {navigate} = this.props.navigation
    const {email,name,isRequest} =this.state
    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ () => navigate('Account')}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Edit Profile</Title>
        </Body>
        <Right>
        <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/>
        </Right>
        </Header>
        <Content padder>
          <Form>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input 
                value={email} 
                editable={!isRequest}
                keyboardType="email-address"
                onChangeText={text => this.setState({email:text})}
                autoCapitalize='none'
              />
            </Item>
            <Item stackedLabel>
              <Label>Name</Label>
              <Input 
                value={name} 
                editable={!isRequest}
                keyboardType="email-address"
                onChangeText={text => this.setState({name:text})}
              />
            </Item>
             
          </Form>
          <Button block style={{margin:15,marginTop:40}} onPress={()=>this._update(navigate)}>
            { isRequest == true ?  <Spinner color="white"/> : null }
            <Text>Update</Text>
          </Button>
        </Content>
        <Footer>
          <FooterTab  navigate={navigate}/>
        </Footer>
        </Container>
    )
  }
}
const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user
})
const mapDispatchToProps = {
  logout:userAction.reset
}

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile)

const styles=StyleSheet.create({
  profileBox:{
    alignItems:'center',
    marginVertical:20
  },
  profileName:{
    marginTop:5,
    fontSize:20,
    fontWeight:'bold'
  },
  profileEmail:{

  },
  actions:{
    padding:50
  },
  actionButton:{
    marginVertical:5
  }
})