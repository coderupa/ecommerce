import React, { Component } from 'react'
import { View } from 'react-native'
import{
  Container,
  Header,
  Content,
  Footer,
  Text,
  Body,
  Title,
  Button
} from 'native-base'

import {
  FooterTab
} from '../components'

import { connect} from 'react-redux';
import API from '../api'
import {cartSum} from '../lib'

class Home extends Component {
  state = {  }
  _getTotalQty(){
    return this.props.cart.reduce((acc, cur) => acc + cur.Qty,0);
  }

  _stripeToken=async ()=>{
    let cardInfo= { // will be in the sanitized and formatted form
      number: "4242424242424242",
      expiry: "06/19",
      cvc: "300"
    }
    let result = await API.stripeToken(cardInfo) 
    console.log(result)
  }
  render() {
    const {navigate} = this.props.navigation
    return (
      <Container>
        <Header>
          <Body>
            <Title>Home</Title>
          </Body>
        </Header>
        <Container>
          <Content>
           <Text>Home Screen</Text>
           {/* <Button onPress={()=>this._stripeToken()}>
             <Text>Stripe Token</Text>
           </Button> */}
          </Content>
        </Container>
        <Footer>
          <FooterTab navigate={navigate} cartSum={cartSum(this.props.cart)}/>
        </Footer>
      </Container>
    )
  }
}


const mapStateToProps = state => ({
  cart: state.cart,
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps,mapDispatchToProps)(Home)