import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View,Alert} from 'react-native';
import { Container,
  Content,
  Header,
  Body,
  Text,
  Left,
  Right,
  Button,
  Form,
  Input,
  Item,
  Label,
  Icon,
  Spinner
} from 'native-base'
import appStyles from '../AppStyle' 
import * as userActions from '../store/user';

import {StackNavigator} from 'react-navigation'
class Login extends Component {
  state = {
    securePassword : true,
    email:'',
    password:''
  }
  
  changePasswordSecure(){
    securePassword=this.state.securePassword
    this.setState({securePassword : !securePassword})
  }

  checkFB(){
    
  }
  async loginFB() {
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1829405577350137', {
        permissions: ['public_profile','email','user_friends'],
      });
    if (type === 'success') {
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      user=await response.json()
      loginInfo={
        'loginVia' :'facebook',
        'token':token
      }
      user= {...user,...loginInfo}
      console.log(user)
      this.props.setUser(user)
    }else if ( type ==='cancel'){
      console.log('canceled')
    }
  }

  async loginGoogle() {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: "418814530801-f3mfnen83m937egs10ie6mli94e03iut.apps.googleusercontent.com",
        iosClientId: "418814530801-hv10id5s858qapejj4tqmghh2kst8jjp.apps.googleusercontent.com",
        scopes: ['profile', 'email'],
      });
  
      if (result.type === 'success') {
        // return result.accessToken;
        console.log(result)
        let token = result.accessToken
        console.log(token)
        loginInfo={
          'loginVia' :'google',
          'token':token
        }

        this.props.setUser(result)

      } else {
        console.log({cancelled: true})
      }
    } catch(e) {
        console.log('catch error' + e.message)
    }
 

  }

  async loginEmail(){
    await this.props.login(this.state.email,this.state.password)
  }

  _showErrorMessage=()=>{
      return (
        Alert.alert(
          'Oopss',
          'Incorrect email and/or password.',
          [
            {text: 'Try Again', onPress: () => this.props.reset()},
          ],
          { cancelable: false }
        )
      )
    }

    
  render() { 
    const {isFailure,isRequest,isSuccess} = this.props.user
    const {navigate} = this.props
    return (
      <Container>
        { isFailure==true ? this._showErrorMessage():null}
        <Content padder style={{backgroundColor:"#FFF"}}>        
          <Form>
            <View style={{padding:15,borderWidth:1,borderColor:'#F1F1F1'}}>
              <Item>
                <Input 
                  placeholder="Email" 
                  keyboardType="email-address" 
                  editable={!isRequest}
                  autoCapitalize='none'
                  onChangeText={(value)=>this.setState({email:value})}
                />
              </Item>
              <Item >
                <Input 
                  placeholder="Password" 
                  secureTextEntry={this.state.securePassword} 
                  editable={!isRequest}
                  onChangeText={(value)=>this.setState({password:value})}
                />
                <Button dark transparent onPress={()=> this.changePasswordSecure()}>
                  <Icon active name={this.state.securePassword ?'ios-eye':'ios-eye-off'} />
                </Button>
              </Item>
              <Button style={{marginTop:20}} full primary onPress={() => this.loginEmail() }  disabled={isRequest}>
                {
                  isRequest == true ?  <Spinner color="white"/> : null
                }
                <Text>Login</Text>
              </Button>
            </View>
            {/* <Button transparent style={{alignSelf:'center'}}>
              <Text>I forgot my password</Text>
            </Button> */}
            
            {/* <Text style={{fontSize:17,marginTop:20,marginBottom:20,textAlign:'center',fontWeight:'bold'}}>  or login with </Text>
            
            <Button full iconLeft onPress={() => null} style={{backgroundColor:'#3b5999'}}  disabled={isRequest}> 
              <Icon name='logo-facebook' />
              <Text style={{fontSize:13,fontWeight:'bold'}}>Login With Facebook</Text>
            </Button>
           
            <Button full iconLeft onPress={() => null} style={{marginTop:10,backgroundColor:'#dd4b39'}}  disabled={isRequest}> 
              <Icon name='logo-google' />
              <Text style={{fontSize:13,fontWeight:'bold'}}>Login With Google</Text>
            </Button> */}

          </Form>
        </Content>

      </Container>
    )
  }
}
 
const mapStateToProps = state => ({
  user:state.user
})
const mapDispatchToProps = {
  login:userActions.login,
  reset:userActions.reset
}
export default connect(mapStateToProps,mapDispatchToProps)(Login)