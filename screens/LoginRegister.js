import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux';
import { 
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  Text,
  Body,
  Title,
  Left,
  Right,
  Icon,
  H2,
  Button } from 'native-base'
import Login from './Login'
import Register from './Register'

class LoginRegister extends Component {
  state = {  }

  _loginSuccess=(navigate)=>{
    navigate('Account')
  }

  render() {
    const {navigate} = this.props.navigation
    // if (this.props.user.isSuccess){
    //   return <Account navigation={this.props.navigation}/>
    // }
    return (
      <Container>
      <View style={{marginTop:15,backgroundColor:'transparent',position:'absolute',zIndex:1,flexDirection:'row'}}>
        <Button transparent  onPress={() => navigate('Home')}>
          <Icon name="ios-arrow-back"/>
        </Button>
      </View>
      <Content>
      <View style={{marginTop:50,marginBottom:20,alignItems:'flex-start',padding:30}}>
        <Text style={{fontSize:14}}>Welcome to</Text>
        <Text style={{fontSize:34,fontWeight:'500'}}>Coderupa</Text>
        <Text style={{backgroundColor:"coral",color:"#FFF",paddingVertical:5,paddingHorizontal:15,fontWeight:'bold'}}>Login | Register</Text>
      </View>
         
      <Tabs initialPage={0}>
        <Tab heading="Login">
          <View style={{padding:15}}>
            <Login loginSuccess={()=>this._loginSuccess(navigate)}/>
          </View>
        </Tab>
        <Tab heading="Register">
          <View style={{padding:15}}>
            <Register />
          </View>
        </Tab>
        
      </Tabs>
      </Content>
    </Container>    
    )
  }
}

const mapStateToProps = state => ({
  user:state.user
})
const mapDispatchToProps = {}
export default connect(mapStateToProps,mapDispatchToProps)(LoginRegister)