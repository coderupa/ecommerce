import React, { Component } from 'react'
import { View, Text as MyText,TouchableOpacity,StyleSheet } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Card,
  CardItem,
  Grid,
  Col,
  Spinner
 } from "native-base"
 import { connect} from 'react-redux'
 import {FooterTab,IconBadge} from '../components'
 import API from '../api'
 import {formatDate,formatMoney,cartSum} from '../lib'

 const OrderHistoryDetail =(props)=>{
   const {id,created_at,items_total,status,payments}=props.data
   return(
    <Card>
      <CardItem>
        <Body>
          <Grid>
            <Col>
              <Text note style={{marginTop:10,fontSize:13}}>Order ID:</Text>
              <Text>#{id}</Text>
  
              <Text note style={{marginTop:10,fontSize:13}}>Date:</Text>
              <Text>{formatDate(created_at)}</Text>
  
              <Text note style={{marginTop:10,fontSize:13}}>Total:</Text>
              <Text>{formatMoney(items_total)}</Text>
            </Col>
            <Col>
              <Text note style={{marginTop:10,fontSize:13}}>Fullfillment Status : </Text>
              <Text>{status}</Text>
  
              <Text note style={{marginTop:10,fontSize:13}}>Payment Status:</Text>
              <Text>{payments ? 'paid':'unpaid'}</Text>
            </Col>
          </Grid>
        </Body>
      </CardItem>
    </Card>
   )
 }

 class OrderHistory extends Component {
  state = {
    isRequest : false,
    data:[]
  }
 

  _getOrderHistory=async()=>{
    await this.setState({isRequest:true,data:[]})
    let result = await API.orderHistory(this.props.user.token)
    await this.setState({isRequest:false,data:result.data})
  } 

  componentDidMount () {
    this._getOrderHistory()
  }
 
  render() {
    const {navigate} = this.props.navigation
    const {data,isRequest} = this.state
    // console.log(this.state)
    // console.log(data)
    return (
      <Container>
        <Header>
        <Left>
        <Button transparent onPress={ () => navigate('Account')}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Order History</Title>
        </Body>
        <Right>
          <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/>
        </Right>
        </Header>
        <Content padder>

            
          {
            isRequest==true ? <Spinner/> :
            data.map((item,index) =>(
              <OrderHistoryDetail key={index} data={item}/>
            ))
          }
        </Content>
        <Footer>
          <FooterTab  navigate={navigate} cartSum={cartSum(this.props.cart)}/>
        </Footer>
        </Container>
    )
  }
}
const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user
})
const mapDispatchToProps = {
}

export default connect(mapStateToProps,mapDispatchToProps)(OrderHistory)

const styles=StyleSheet.create({
  
})