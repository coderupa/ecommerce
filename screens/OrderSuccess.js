import React, { Component } from 'react'
import { View,StyleSheet,Animated } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Card,
  CardItem,
  ListItem,
  Radio,
  CheckBox,
  Picker
 } from "native-base"
 
import Animation from 'lottie-react-native';
import LottieView from 'lottie-react-native';


 class Order extends Component {
  state = {
    progress: new Animated.Value(10),
  }

  componentDidMount () {
    this.animation.play();
  }
  
  render() {
    const {navigate} = this.props.navigation
    return (
      <Container>
        <View style={{alignItems:'center',justifyContent:'center',alignItems: 'center',flex:1}}>
          <Text style={{fontSize:22,fontWeight:'bold'}}>Order Succesfully</Text>
          <Text>Your Order has been placed</Text>
          <View style={{  width: 150,height: 150,}}>
          <Animation
              ref={animation => { this.animation = animation; }}
              style={{
                width: 150,
                height: 150,
                margin:0,
                padding:0,
              }} 
              source={require('../assets/success.json')}
              loop={true}
            /> 
            </View>
            <Button success blocked style={{marginTop:20,alignSelf:'center'}} onPress={()=>navigate('Home')}>
            <Text> Continue Shopping </Text>
            </Button>
        </View>
        </Container>
    )
  }
}
 

export default Order  

const styles=StyleSheet.create({
  
})