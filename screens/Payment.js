import React, { Component } from 'react'
import { View,StyleSheet } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  ListItem,
  Radio,
  CheckBox,
  Picker,
  Spinner
 } from "native-base"
 import { CreditCardInput} from "react-native-credit-card-input";
 import {CheckoutStep} from '../components'
 import API from '../api'
 import { connect} from 'react-redux'
 import * as cartActions from '../store/cart';

 class Payment extends Component {
  state = {
    selectedpaymentMethod: 'cc',
    paymentMethods:[],
    cardValid:false,
    cardInfo:'',
    isRequest:false,
    isFailure:false,
    errorMessage:''
  }

  _getPaymentMethods=async ()=>{
    await this.setState({isRequest:true})
    let result=await API.paymentMethod()
    if(result.status ==true){
      await this.setState({ paymentMethods:result.data})
    }
    await this.setState({isRequest:false})
  }

  _submitOrder=async()=>{
    this.setState({isRequest:true,isFailure:false})
 
     // 1. create order
     const shippingForm=this.props.form.shippingForm.values
     let items=[]
     this.props.cart.map(item => (
        items=[...items,{'product_id':item.id,"quantity":item.Qty}])
     )
     let shipping_id=shippingForm.shipping_method
     let shipping_address={...shippingForm}
     delete shipping_address.shipping_method    
     let billing_address=shipping_address
     let userToken = this.props.user.token
     let result = await API.createOrder(items,shipping_id,shipping_address,billing_address,userToken)
     
     //2. Create Payment if Order Success
     if (result.status == true){
      const order_id=result.data.id
      const {cardInfo,selectedpaymentMethod}=this.state 
      const token = this.props.user.token
       if(selectedpaymentMethod=='cc'){
          result = await API.createPaymentStripe(order_id,cardInfo,token)
       }else{
         result =await API.createPayment(selectedpaymentMethod,order_id,token)
       }

       if (result.status==true){
         await this.props.reset()
         await this.props.actionStep()
       }else{
        await this.setState({
          isRequest:false,
          isFailure:true,
          "errorMessage":result.errors.message})
       } 
     }else{
        await this.setState({
          isRequest:false,
          isFailure:true,
          "errorMessage":result.errors.message})
     }

  }

  _renderPaymentMethods(){
    let {selectedpaymentMethod}=this.state
    let {paymentMethods} = this.props
    paymentMethods=[{id:'cc',name:'Credit Card'},...paymentMethods]
    let cardValid= selectedpaymentMethod =='cc' ? true :false
    return(
      paymentMethods.map(item=>{
        return(
        <ListItem key={item.id} style={{borderBottomWidth:0}}>
          <CheckBox checked={item.id ==selectedpaymentMethod} onPress={()=>this.setState({selectedpaymentMethod:item.id,cardValid:cardValid})} />
          <Body>
            <Text>{item.name}</Text>
          </Body>
          </ListItem>
      )})
    )  
  }
 
  _renderCreditCard(){
    return (
      <View style={{backgroundColor:"#FFF",padding:10}}>
        <CreditCardInput onChange={(value)=>this.setState({cardValid:value.valid,cardInfo:value.values})} />
      </View>
    )
  }

  _renderErrorMessage(message){
    const {navigate} = this.props.navigation
    return (
      <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
          <Icon name="ios-warning-outline" style={{fontSize:100,marginBottom:5,color:'red'}}/>
          <Text style={{fontSize:20,fontWeight:'bold'}}>Oppps </Text>
          <Text>{message}</Text>
          <Button rounded style={{alignSelf:'center',marginTop:30}} onPress={()=>this._submitOrder()}>
            <Text>Try Again</Text>
          </Button>
          <Button transparent style={{alignSelf:'center',marginTop:5}} onPress={()=>navigate('Home')}>
            <Text> ← Back to shopping</Text>
          </Button>
        </View>
    )
  }
  render() {
    
    const {paymentMethods,selectedpaymentMethod,isRequest,isFailure} =this.state
    
    if (isFailure == true ) {return this._renderErrorMessage() }
 
    return (
      <Container>
        {/* <Header>
        <Left>
        <Button transparent onPress={ () => goBack()}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Payment</Title>
        </Body>
        <Right>
        </Right>
        </Header> */}
          <Content padder style={{backgroundColor:"#F1F1F1"}}>
            {/* <CheckoutStep currentPosition={1}/> */}
              <View style={{backgroundColor:"#FFF",paddingVertical:10,marginVertical:10}}>
                <Text style={{fontWeight:"bold",marginLeft:15,marginRight:0}}>Payment Method : </Text>
                  {this._renderPaymentMethods() }
              </View>

              { selectedpaymentMethod==="cc" ? this._renderCreditCard() :null }
               
          </Content>
          <Button full iconRight onPress={()=>this._submitOrder()} disabled={!this.state.cardValid}>
            {this.state.isRequest ? <Spinner color='white'/> :null }
            <Text style={{fontWeight:'bold'}}>Next</Text>
            <Icon name="md-arrow-round-forward"/>
          </Button>
        </Container>
    )
  }
}

const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user,
  form:state.form
})

const mapDispatchToProps = {
  reset:cartActions.reset
}

export default connect(mapStateToProps,mapDispatchToProps)(Payment)

const styles=StyleSheet.create({
  
})