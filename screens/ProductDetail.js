import React, { Component } from 'react'
import { View,Dimensions,Image,StyleSheet,TextInput } from 'react-native'
import { 
  Container,
  Header,
  Content,
  Text,
  Body,
  Title,
  Left,
  Right,
  Button,
  Icon,
  Input
 } from 'native-base'

import {QuantityInput} from '../components'
import {formatMoney} from '../lib'
const productPlaceholder= require('../assets/product-placeholder.jpg')

export default class ProductDetail extends Component {
  state = {
    productWidth:null,
    productHeight:null,
    quantity:1,
  }
  
  componentDidMount() {
    Dimensions.addEventListener('change',this._updateProductSize)
    this._updateProductSize()
  }

  _updateProductSize=(dimensions)=>{
   if (dimensions){
    const {width,height} = dimensions
   }
   const {width,height} = Dimensions.get('window')
    let productWidth=width
    let productHeight=0.6 * productWidth
    this.setState({
      productWidth,
      productHeight
    })
  }
  render() {
    const {productWidth,productHeight}=this.state
    const {id,productName,price,image,productDescription,updateCart} = this.props.product
    return (
      <Container>
      <View style={{marginTop:15,backgroundColor:'transparent',position:'absolute',zIndex:1,flexDirection:'row'}}>
        <Button transparent  onPress={() => this.props.closeDetail()}>
          <Icon name="ios-arrow-back"/>
        </Button>
      </View>
        <Content  style={{backgroundColor:"#F1F1F1"}}>
          <View>
            <Image 
              style={styles.productImage} 
              source={{uri:image}} 
              defaultSource={productPlaceholder}
              style={{width:productWidth,height:productHeight}}
            />
            <View style={[styles.productInfo,{padding:10,backgroundColor:"#FFF"}]}>
              <Title style={styles.productName}>{productName}</Title>
              <Title style={styles.productPrice}>{formatMoney(price)}</Title>
            </View>
          </View>

          <View style={styles.productBox}>
            <Text style={{fontSize:13,fontWeight:'bold',marginBottom:15}}>Description</Text>
            <Text style={styles.productDescText}>{productDescription}</Text>
          </View>

          <View style={styles.productBox}>
            <Text  style={{fontSize:13,fontWeight:'bold',marginBottom:15}}>Quantity</Text>
            <View>
              <QuantityInput value={1} onChange={(value)=>this.setState({quantity:value})}/>
            </View>
          </View>

        </Content>
        <Button full onPress={()=>updateCart({id,productName,price,image,productDescription,"Qty":this.state.quantity})} > 
          <Text>Beli</Text>
        </Button>
      </Container>
    )
  }
}

const styles=StyleSheet.create({
  productImage:{},
  productName:{
    color:'#000'
  },
  productPrice:{
    color:'#000',
    fontSize:13
  },
  productBox:{
    marginTop:10,
    padding:15,
    backgroundColor:"#FFF",
    flex:1
  },
  productDescText:{}
})