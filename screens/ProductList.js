import React, { Component } from 'react'
import { View,Image,Dimensions,FlatList,StyleSheet,TouchableOpacity,Modal,Platform } from 'react-native'
import{
  Container,
  Header,
  Content,
  Footer,
  Text,
  Body,
  Title,
  Left,
  Right,
  Button,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Grid,
  Row,
  Col,
  Input,
  Spinner,
  Fab,
  FooterTab,
  Badge,
  ListItem,
  CheckBox,
  Item,
  Picker
 
} from 'native-base'
import {IconBadge} from '../components'
import ProductDetail from '../screens/ProductDetail' 
import ShopFilter from './ShopFilter'
import ShopSort from './ShopSort'
import API from '../api'
import {formatMoney,cartSum} from '../lib'
import _ from 'lodash'

//redux stuff
import { connect} from 'react-redux'
import * as productsActions from '../store/products';
import * as cartActions from '../store/cart';
import {getProductState} from '../store/selector'


class ProductItem extends React.PureComponent {
  state={
    showDetail:false,
    selectedProduct:{},
    other:'test'
  }
  
  _closeDetail=()=>{
    this.setState({
      showDetail:false
    })
  }

  _selectProduct=(product)=>{
    this.setState({
      showDetail:true,
      selectedProduct:product
    })
  }

  render(){
    const {id,name,price,images,description,updateCart} = this.props
    const {width,height} = Dimensions.get('window')
    const productWidth =  0.5 * (width-24)  
    const productHeight = 0.8 * productWidth
    const productPlaceholder= require('../assets/product-placeholder.jpg')
    return(
      <View>
        <Modal visible={this.state.showDetail} onRequestClose={this._closeDetail} animationType="slide">
          <ProductDetail closeDetail={this._closeDetail} product={this.state.selectedProduct} updateCart={updateCart}/>
        </Modal>
        <TouchableOpacity 
          style={styles.productWrapper} 
          onPress={()=> this._selectProduct(this.props)}
          >
          <Image 
            source={{uri:images[0]}}
            defaultSource={productPlaceholder}
            style={{width:productWidth,height: productHeight
            
            }} 
          />
          <View  style={[styles.productInfo,{width: productWidth}]}>
            <Text style={[styles.productTitle]}>{name}</Text>
            <Text style={styles.productPrice}>{formatMoney(price)}</Text>
            <Button small danger full onPress={()=>updateCart({id,name,price,images,description,"Qty":1})}>
              <Text>Buy</Text>
            </Button>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

 

class ProductList extends Component {
  state = {
    products:[],
    page:0,
    remaining:0,
    fetching:false,
    refreshing:false,
    showSort:false,
    showFilter:false

  }


  async componentDidMount() {
    this.props.fetchProducts()
  }
 
  // _fetchProducts=async()=>{
  //   const {page} =this.state
  //   let products = await API.readProducts(page+1)
  //   // console.log(await products.data.length)
  //   // console.log(`_fetchProducts-${page} || length : ${products.length}`)
  //   if(products.data.length == 0 ){
  //     await this.setState({refreshing: false,fetching:false })
  //   }else{
  //     await this.setState({
  //       products: [...this.state.products,...products.data],
  //       refreshing:false,
  //       fetching:false,
  //       page:page+1,
  //       remaining:products.count
  //     })
  //   }
  // }

  _handleRefresh=()=>{
    console.log('handle refresh')
    this._handleLoadMore()
  }

  _handleLoadMore= (info)=>{
    // console.log('_handleLoadMore')
    const {data,remaining,fetching,count} = this.props.products
    // console.log(products.length,count)
    if (data.length <= count && fetching == false) { 
      // console.log('mananan')
      this.props.fetchProducts()
    }
  }
 
 _hideModal=(modal)=>{
   let obj={}
   obj[modal]=false
   this.setState(obj)
 }

  render() {
    const {navigate} = this.props.navigation
    const {data,page,count,fetching,sort,filter}=this.props.products
    // console.log(this.props)
    return (
      <Container>
        <Header>
            <Left>
            <Button transparent onPress={ () => navigate('Home')}>
              <Icon name="ios-arrow-back"/>
            </Button>
          </Left>
          <Body>
            <Title>Product List</Title>
          </Body>
          <Right>
            <IconBadge name="ios-cart" number={cartSum(this.props.cart)} onPress={()=>navigate('Cart')}/> 
         </Right>
        </Header>
        {/* <Content> */}
        <View style={{flex:1}}>
          <Modal visible={this.state.showSort} animationType="slide" onRequestClose={()=>this._hideModal('showSort')} >
            <ShopSort 
              onBackButtonPress={()=>this._hideModal('showSort')} 
              sortBy={sort}
              action={(value)=>this.props.sortBy(value)}
              />
          </Modal>

          <Modal visible={this.state.showFilter} animationType="slide" onRequestClose={()=>this._hideModal('showFilter')} >
            <ShopFilter 
              onBackButtonPress={()=>this._hideModal('showFilter')} 
              action={(value)=>this.props.filter(value)}
              filter={filter}
            />
          </Modal>

          <View style={{flexDirection:'row',borderBottomWidth:1,borderBottomColor:'#CCC'}}>
            <View style={{flex:1,borderRightColor:"#CCC",borderRightWidth:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <Button transparent full onPress={()=>this.setState({showSort:true})}>
                <Icon name="ios-checkmark-circle" style={{color:'green',fontSize:16}}/>
                <Text>Urutkan</Text>
              </Button>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <Button transparent full onPress={()=>this.setState({showFilter:true})}>
                <Icon name="ios-checkmark-circle" style={{color:'green',fontSize:16}}/>
                <Text>Filter</Text>
              </Button>
            </View>
          </View>
          <FlatList
            data={data}
            // extraData={this.state.products}
            renderItem={({item})=> (
              <ProductItem updateCart={this.props.updateCart} {...item}/>
            )}
            keyExtractor={item => item.sku}
            onEndReached={this._handleLoadMore}
            onEndReachedThreshold={50}
            onRefresh={this._handleRefresh}
            refreshing={fetching}
            numColumns={2}
          />
        </View>  
        {/* </Content>   */}

        <View style={{backgroundColor:'firebrick',paddingVertical:5}}>
          <Text style={{fontSize:12,textAlign:'center',fontWeight:'bold',color:"#FFF"}}>
            Page #{page} : Load {data.length} from {count}
          </Text>
        </View>  

        {/* <Footer>
          <FooterTab>
            <Button>
              <Icon name="md-funnel" />
              <Text>Sort</Text>
            </Button>
            <Button onPress={()=>null}>
              <Icon name="ios-switch" />
              <Text>Filter</Text>
            </Button>
          </FooterTab>         
        </Footer> */}
      </Container>
   
    )
  }
}

// const queryProducts=(filterPrice,sortBy,products)=>{
//   console.log('queryProducts')
//   let result={...products}
//   if(sortBy){
//     console.log('sort ' + sortBy)
//     let sortedProducts=_.orderBy(result.data, ['price'], [sortBy]);
//     result={...result,data:sortedProducts}
//   }

//   if(filterPrice.max>0){
//     console.log('filter ')
//     let filteredProducts=_.filter(result.data,(item)=>(item.price >= filterPrice.min && item.price <= filterPrice.max))
//     result={...result,data:filteredProducts}
//   }


//   console.log(sortBy)
//     // this.setState({products:sorted})
//   // return sortedProducts
//   return result
 
// }

const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user,
  // products:state.products,
  // products:queryProducts(state.products.filter,state.products.sort,state.products)
  products:getProductState(state)
})
const mapDispatchToProps = {
  fetchProducts:productsActions.fetch,
  updateCart:cartActions.update,
  sortBy:productsActions.sort,
  filter:productsActions.filter
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductList)

const styles=StyleSheet.create({
  productWrapper:{
    flex:1,
    margin:5,
    marginBottom:15,
    borderWidth:1,
    borderColor:'#F1F1F1',
    backgroundColor:"#FFF",
    shadowColor: "#000",
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 0.2,
    shadowRadius:  1,
    flexDirection:'column',
    flexWrap:'wrap'
  },
  
  productInfo:{
    flexDirection:'column',
    padding:10,
    flex:1,
    flexWrap:'wrap'
    
  },
  
  productTitle:{
    flexDirection:'row',
    fontWeight:'bold',
    flex:1,
    flexWrap:'wrap'
  },
  productPrice:{
    fontSize:13,
    paddingVertical : 5,
    marginBottom:5
  }
  
})