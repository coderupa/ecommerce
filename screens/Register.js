import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View,Alert } from 'react-native';
import { 
  Container,
  Content,
  Header,
  Body,
  Text,
  Left,
  Right,
  Button,
  Form,
  Input,
  Item,
  Label,
  Icon,
  Spinner
} from 'native-base'
import * as authActions from '../store/auth';
import * as userActions from '../store/user';
import API from '../api'
 

class Register extends Component {
  state = {
    securePassword : true,
    email:'',
    name:'',
    password:'',
    isRequest:false,
    isFailure:false,
    isSuccess:false,
  }

  _changePasswordSecure(){
    securePassword=this.state.securePassword
    this.setState({securePassword : !securePassword})
  }

  _checkToken=async ()=>{
    let token = this.props.auth.token
    if (token==''){
     await this.props.getToken()
     token = this.props.auth.token
       if (token=='') { return }
    }
    // return true
  }

  _register=async()=>{
    this.setState({isRequest:true},
       async ()=>{
        await this.props.validateToken()
        let token=this.props.auth.token
        // alert(token)
        let response=await API.register(
          this.state.email,
          this.state.name,
          this.state.password,
          "",
          token
        )
        if (response.status==true){
          this.setState({
            isSuccess:true
          })
        }else{
          this.setState({
            isFailure:true,
            errorMessage:response.errors[0].message
          })
        }
      }
    )
  }

  _showErrorMessage=()=>{
    return (
      Alert.alert(
        'Oopss',
        this.state.errorMessage,
        [
          {text: 'Try Again', onPress: () =>  this.setState({isFailure:false,isRequest:false})},
        ],
        { cancelable: false }
      )
    )
  }

  _showSuccessMessage=()=>{
    return (
      Alert.alert(
        'Success',
        'Your Account has been created, now go login buddy',
        [
          {text: 'Ok', onPress: () =>  this.setState({isSuccess:false,isRequest:false})},
        ],
        { cancelable: false }
      )
    )
  }

  render() {
    const {isFailure,isRequest,isSuccess} = this.state
    return (
      <Container>
        { isFailure==true ? this._showErrorMessage():null}
        { isSuccess==true ? this._showSuccessMessage():null}
        <Content padder style={{backgroundColor:'#fff'}}>
          <Form>
          <View style={{padding:15,borderWidth:1,borderColor:'#F1F1F1'}}>
            <Item>
              <Input 
                placeholder="Email" 
                keyboardType="email-address" 
                autoCapitalize='none'
                onChangeText={(value)=>this.setState({email:value})}
                />
            </Item>

            <Item>
              <Input 
                placeholder="Name" 
                autoCapitalize='none'
                onChangeText={(value)=>this.setState({name:value})}
              />
            </Item>

            <Item>
              <Input 
                placeholder="Password" 
                secureTextEntry={this.state.securePassword}
                onChangeText={(value)=>this.setState({password:value})} />
              <Button dark transparent 
                onPress={()=> this._changePasswordSecure()}>
                <Icon active name={this.state.securePassword ?'ios-eye':'ios-eye-off'} />
              </Button>
            </Item>

            <Item>
              <Input placeholder="Confirm Password" secureTextEntry={this.state.securePassword} />
              <Button dark transparent 
                onPress={()=> this.changePasswordSecure()}>
                <Icon active name={this.state.securePassword ?'ios-eye':'ios-eye-off'} />
              </Button>
            </Item>
            
            <Button style={{marginTop:20}} full primary onPress={()=>this._register()} >
              { isRequest == true ?  <Spinner color="white"/> : null }
              <Text>Register</Text>
            </Button>
          </View>

            {/* <Text style={{fontSize:17,marginTop:20,marginBottom:20,textAlign:'center',fontWeight:'bold'}}>or register with</Text>
            
            <Button full iconLeft onPress={() => this.loginFB()} style={{backgroundColor:'#3b5999'}}> 
              <Icon name='logo-facebook' />
              <Text style={{fontSize:13,fontWeight:'bold'}} >Register With Facebook</Text>
            </Button>
           
            <Button full warning iconLeft onPress={() => this.loginFB()} style={{marginTop:10,backgroundColor:'#dd4b39'}}> 
              <Icon name='logo-google' />
              <Text style={{fontSize:13,fontWeight:'bold'}}>Register With Google</Text>
            </Button> */}
            
          </Form>           
        </Content>

      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth:state.auth
})
const mapDispatchToProps = {
  validateToken:authActions.validateToken,
  register:userActions.register
}
export default connect(mapStateToProps,mapDispatchToProps)(Register)