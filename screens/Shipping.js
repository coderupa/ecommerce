import React, { Component } from 'react'
import { View,StyleSheet,TextInput } from 'react-native'
import { 
  Container,
  Content,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon,
  Button,
  Text,
  Badge,
  Footer,
  Tab,
  Tabs,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Card,
  CardItem,
  ListItem,
  Radio,
  CheckBox,
  Picker,
  Spinner,
  List,
 } from "native-base"

 import {FooterTab,IconBadge,CheckoutStep} from '../components'
 import API from '../api'
 import { connect} from 'react-redux'
 import { Field, reduxForm } from 'redux-form'
//  import  RenderField  from '../components/RenderField'




//  const AddressItem=(props)=>{
//    console.log(props)
//   return(
//     <View>
//       <Text>{props.full_name}</Text>  
//       <Text>{props.email}</Text>  
//       <Text>{props.phone_number}</Text>  
//       <Text>{props.address1}</Text>  
//       <Text>{props.country}- {props.state} - {props.city}</Text>
//       <Text>{props.postal_code}</Text>  
//     </View>
//   )
//  }

 const RenderInput=(props)=>{
  const { input, label, type,keyboardType, meta: { touched, error, warning },placeholder }=props
  var hasError= false;
  if(error !== undefined){
    hasError= true;
  }
  return( 
    <View>
      <Item  stackedLabel  style= {{marginTop:10}} error= {hasError}>
        <Label style={{fontWeight:'bold',marginBottom:8,fontSize:13}}>{label}</Label>
        <Input 
          {...input} 
          placeholder={placeholder} 
          keyboardType={keyboardType}
          style={{borderWidth:1,borderColor:'#CCC',paddingHorizontal:10}}
          />
      </Item> 
        {hasError ? <Text style={{fontSize:14,fontStyle:'italic',marginBottom:10,marginLeft:15}}>{error}</Text> :null}
    </View>
  )
}

const RenderOptions=({input,meta,options})=>{
  return(
    options.map(item=>{
      return(
      <ListItem key={item.id} style={{borderBottomWidth:0}}>
        <CheckBox checked={item.id ==input.value} onPress={(value)=>input.onChange(item.id)} />
        <Body>
          <Text>{item.name}</Text>
        </Body>
      </ListItem>
    )})
  )
}

 class Shipping extends Component {
  state = {
    isRequest:false,
    isSuccess:false,
    isFailure:false,
    shippingOptions:[],
    selectedShippingMethod:''
  }

  // _renderShippingMethods(){
  //   let {shippingMethods}=this.props
     
  //     return(
  //       shippingMethods.map(item=>{
  //         return(
  //         <ListItem key={item.id} style={{borderBottomWidth:0}}>
  //           <CheckBox checked={item.id ==this.state.selectedShippingMethod} onPress={()=>this.setState({selectedShippingMethod:item.id})} />
  //           <Body>
  //             <Text>{item.name}</Text>
  //           </Body>
  //         </ListItem>
  //       )})
  //     )
  // }

  _submit=()=>{
    this.setState({isRequest:true})
    // let shipping_address=address
    // let billing_address=shipping_address
    // let item=this.props.cart
    // let userToken = this.props.user.token
    // let result = API.createOrder(items,shipping_address,billing_address,userToken)
    // if (result.status == true){
      //   await this.setState({
        //     order_id:result.order_id,
        //     currentPosition:1
        //   })
        // }
    this.setState({isRequest:false})
  }

  render() {
    // const {navigate,goBack} = this.props.navigation
    const {isSuccess,isRequest,shippingOptions} = this.state
    const { error, handleSubmit, pristine, reset, submitting,actionStep,shippingMethods } = this.props
    // console.log(this.props)
    let addressData={
      "full_name": "Eddard Stark",
      "email": "theuser@yahoo.com",
      "country": "Indonesia",
      "state": "The North",
      "city": "Winterfell",
      "address1": "Winterfell Castle, 1",
      "address2": "",
      "postal_code": "20221",
      "phone_number": "081537123743",
      "alternate_phone_number": "",
      "vat": "",
      "user_id": 350979,
      "application_id": 340041,
      "id": 353944,
      "updated_at": "2017-12-26T17:12:01.396Z",
      "created_at": "2017-12-26T17:12:01.396Z"
  }
    return (
      <Container>
        {/* <Header>
        <Left>
        <Button transparent onPress={ () => navigate(this.props.navigation.state.params.from)}>
          <Icon name="ios-arrow-back"/>
        </Button>
        </Left>
        <Body>
          <Title>Shipping</Title>
        </Body>
        <Right>
        </Right>
        </Header> */}
          <Content style={{backgroundColor:"#F1F1F1"}}>
            {/* <CheckoutStep currentPosition={0}/> */}
            <Form>
              <View style={{padding:0}}>
                {/* <View style={{backgroundColor:"#FFF",paddingVertical:0,marginVertical:10,flexDirection:"row",alignItems:'center'}}> */}
                <View style={{backgroundColor:"#FFF",paddingVertical:10,marginVertical:10}}>
                  <Text style={{fontWeight:"bold",marginLeft:10,marginRight:0}}>Shipping Method : </Text>
                   {/* {this._renderShippingMethods()} */}
                   <Field 
                    name="shipping_method" 
                    component={RenderOptions} 
                    options={shippingMethods}
                    label="Full Name" />
                </View>

                <View style={{backgroundColor:"#FFF",padding:10}}>
                  <Text style={{fontWeight:"bold"}}>Shipping Address</Text>
                    {/* <Form> */}
                      <Field 
                        name="full_name" 
                        component={RenderInput} 
                        placeholder="Full Name" 
                        label="Full Name" />
                        
                      <Field 
                        name="email" 
                        component={RenderInput} 
                        placeholder="Email Address" 
                        label="Email Address" 
                        autoCapitalize="none" />

                      <Field 
                        name="phone_number" 
                        component={RenderInput} 
                        placeholder="Phone Number" 
                        keyboardType="numeric"
                        label="Phone Number" />

                      <Field 
                        name="country" 
                        component={RenderInput} 
                        placeholder="Country" 
                        label="Country"  />
                    
                      <Field 
                        name="state" 
                        component={RenderInput} 
                        placeholder="State" 
                        label="State"  />

                      <Field 
                        name="city" 
                        component={RenderInput} 
                        placeholder="City" 
                        label="City"  />

                      <Field 
                        name="address1" 
                        component={RenderInput} 
                        placeholder="Street Name" 
                        label="Address"  />
                        
                      <Field 
                        name="postal_code" 
                        component={RenderInput} 
                        placeholder="Postal Code" 
                        label="Postal Code"  />

                    {/* </Form> */}
                </View>

              </View>
              </Form>
          </Content>
          {/* <Button full iconRight > */}
          <Button disabled={submitting}  full iconRight onPress={()=>actionStep()}>
          {/* <Button full iconRight onPress={()=>navigate('Payment')}> */}
            <Text style={{fontWeight:'bold'}}>Next</Text>
            <Icon name="md-arrow-round-forward"/>
          </Button>
        </Container>
    )
  }
}


/*
Shipping = reduxForm({
  // a unique name for the form
  form: 'shipping',
  // validate
})(Shipping)

const mapStateToProps = state => ({
  cart: state.cart,
  user:state.user
})
const mapDispatchToProps = {
}

export default connect(mapStateToProps,mapDispatchToProps)(Shipping)
*/

// export default reduxForm({
//   form: 'Shipping',
//   validate
// })(Shipping);

const validate = values => {
  const error= {};
  error.email= '';
  error.full_name= '';
  var ema = values.email;
  var nm = values.full_name;

  if(values.email === undefined){
    ema = '';
  }
  if(values.full_name === undefined){
    nm = '';
  }
  if(ema.length < 8 && ema !== ''){
    error.email= 'too short';
  }
  if(!ema.includes('@') && ema !== ''){
    error.email= '@ not included';
  }

  if(nm.length > 8){
    error.full_name= 'max 8 characters';
  }
return error;
};


const mapStateToProps = state => ({
  // flightsSearch: state.flightsSearch,
});

const mapDispatchToProps ={}

Shipping = connect(
    mapStateToProps,
    mapDispatchToProps
)(Shipping);

export default reduxForm({
    form: 'shippingForm',
    validate,
    destroyOnUnmount: false
})(Shipping);

const styles=StyleSheet.create({
  
})