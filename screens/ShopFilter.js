import React, { Component } from 'react'
import { View,Modal,Slider } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Icon,
  Button,
  Body,
  Title,
  Left,
  Right,
  List,
  ListItem,
  Radio,
  Input,
  Item,
  Label,
  Form,
  Toast
} from 'native-base' 

export default class ShopFilter extends Component {
  state = { 
    min:this.props.filter.min,
    max:this.props.filter.max
   }
 
  _submitFilter=()=>{
    let {min,max} =this.state
    if (Number(min) > (Number(max))){
      this.setState({errorMsg:'Harga Maximal harus lebih besar dari harga minimal'})
    }else{
      this.setState({errorMsg:''})
      this.props.action(this.state)
      this.props.onBackButtonPress()
    }

  } 
  render() {
    const {onBackButtonPress,selected,selectFilter,action,filter}=this.props
    return (
      <Container>
      <Header>
        <Left>
          <Button transparent onPress={()=>onBackButtonPress()}>
           <Icon name="ios-arrow-back"/>
          </Button>
        </Left>
        <Body>
          <Title>Filter</Title>
        </Body>
        <Right/>
      </Header>
        <Content padder>
          <Text style={{fontSize:15,fontWeight:'bold'}}>Rentang Harga : </Text>
          <Form>
            <Item stackedLabel>
              <Label>Harga Minimum</Label>
              <Input placeholder="Contoh 1000" value={this.state.min.toString()} onChangeText={(value)=>this.setState({'min':Number(value)})}/>
            </Item>
            <Item stackedLabel>
              <Label>Harga Maximum</Label>
              <Input placeholder="Contoh 5000" value={this.state.max.toString()} onChangeText={(value)=>this.setState({'max':Number(value)})}/>
            </Item>

            <Text style={{color:'red',marginVertical:10}}>{this.state.errorMsg}</Text>
            <Button full onPress={()=>this._submitFilter()}>
              <Text>Filter</Text>
            </Button>
          </Form>
        </Content>
    </Container>        
    )
  }
}

// const ShopFilter=({onBackButtonPress,selected,selectFilter,action=null})=>(
//   <Container>
//    <Header>
//      <Left>
//        <Button transparent onPress={()=>onBackButtonPress()}>
//         <Icon name="ios-arrow-back"/>
//        </Button>
//      </Left>
//      <Body>
//        <Title>Filter</Title>
//      </Body>
//      <Right/>
//    </Header>
//      <Content padder>
//        <Text style={{fontSize:15,fontWeight:'bold'}}>Rentang Harga : </Text>
//        <Form>
//          <Item stackedLabel>
//            <Label>Harga Minimum</Label>
//            <Input placeholder="Contoh 1000"/>
//          </Item>
//          <Item stackedLabel>
//            <Label>Harga Maximum</Label>
//            <Input placeholder="Contoh 5000"/>
//          </Item>
//          <Button full onPress={()=>action}>
//            <Text>Filter</Text>
//          </Button>
//        </Form>
//      </Content>
//  </Container>  
// )

// export default ShopFilter