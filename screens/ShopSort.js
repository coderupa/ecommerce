import React, { Component } from 'react'
import { View,Modal,Slider } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Icon,
  Button,
  Body,
  Title,
  Left,
  Right,
  ListItem,
  Radio,
} from 'native-base' 

let options=[
  {label:'Termurah',method:'asc'},
  {label:'Termahal',method:'desc'},
]

const ShopSort=({onBackButtonPress,sortBy,action})=>(
  <Container>
   <Header>
     <Left>
       <Button transparent onPress={()=>onBackButtonPress()}>
        <Icon name="ios-arrow-back"/>
       </Button>
     </Left>
     <Body>
       <Title>Sort</Title>
     </Body>
     <Right/>
   </Header>
     <Content>
       {
         options.map((item,index) => (
          <ListItem key={index} onPress={()=>{ action(item.method);onBackButtonPress()}}>
            <Text>{item.label}</Text>
            <Right>
              <Radio selected={item.method == sortBy ? true:false} />
            </Right>
          </ListItem>
         ))
       }
     </Content>
 </Container>  
)

export default ShopSort