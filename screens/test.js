import React, { Component } from 'react'
import { View,Modal,Slider } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Icon,
  Button,
  Body,
  Title,
  Left,
  Right,
  List,
  ListItem,
  Radio,
  Input,
  Item,
  Label,
  Form,
} from 'native-base' 

import ShopFilter from './ShopFilter'
import ShopSort from './ShopSort'

import API from '../api'
// import crypto from 'crypto'
// var  crypto = require('crypto');
// import Marketcloud from 'marketcloud-node'
// var Marketcloud = require('marketcloud-node');

//  import Hashes from 'jshashes'
//  var Marketcloud = require('marketcloud-js');
//  console.log(Marketcloud)

// var Hashes = require('jshashes')
// sample string
// var str = 'This is a sample text!'
// new SHA1 instance and base64 string encoding
// var SHA1 = new Hashes.SHA256().b64(str)
// output to console
// console.log('SHA1: ' + SHA1)


 
 

class TestScreen extends Component {
  state = {
    showSort:false,
    showFilter:false
  }
  componentDidMount =async() => {
    // let result = await API.authenticate('itpanca@gmail.com','12345678')
    // console.log(result)
    // let publicKey='405bfee4-79a2-4845-9519-7f06693e0c52'
    // let secretKey='3ZklE7etK3sa06u7d7VIwiVOOxXw0TSohmFYgXfJ2Ok-'
    
    //marketcloud js
    // var marketcloud = new Marketcloud.Client({
    //   public_key : '405bfee4-79a2-4845-9519-7f06693e0c52'
    // })

    
    //get token
/*
    let publicKey='405bfee4-79a2-4845-9519-7f06693e0c52'
    let secretKey='3ZklE7etK3sa06u7d7VIwiVOOxXw0TSohmFYgXfJ2Ok-'
    let currentTime = Date.now()
    let hash_secretKey = new Hashes.SHA256().b64(secretKey+currentTime)
    console.log(currentTime)
    console.log(publicKey)
    console.log(hash_secretKey)
*/
    // get products
    // let response = await fetch("https://api.marketcloud.it/v0/products", {
    //   method: 'GET',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //     'Authorization': publicKey+':'+ hash_secretKey
    //   }
    // });

    // console.log(await response.json())

  }
  hideSort=()=>this.setState({showSort:false})
  hideFilter=()=>this.setState({showFilter:false})
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Header</Title>
          </Body>
        </Header>
        

        <Modal visible={this.state.showSort} animationType="slide" onRequestClose={()=>this.hideModal2()} >
          <ShopFilter hideModal={this.hideSort} />
        </Modal>

        <Modal visible={this.state.showFilter} animationType="slide" onRequestClose={()=>this.hideModal()} >
          <ShopSort hideModal={this.hideFilter} selectFilter={()=>alert('test')}/>
        </Modal>

        <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
          <Icon name="ios-warning-outline" style={{fontSize:100,marginBottom:5,color:'red'}}/>
          <Text style={{fontSize:20,fontWeight:'bold'}}>Oppps </Text>
          <Text>Keep calm, ligh and fire to refresh</Text>
          {/* <Button rounded style={{alignSelf:'center',marginTop:30}} onPress={()=>this.setState({showModal:true})}>
            <Text>Try Again</Text>
          </Button> */}
          <Button rounded style={{alignSelf:'center',marginTop:30}} onPress={()=>this.setState({showSort:true})}>
            <Text>Filter</Text>
          </Button>
          <Button rounded style={{alignSelf:'center',marginTop:30}} onPress={()=>this.setState({showFilter:true})}>
            <Text>Sort</Text>
          </Button>
          <Button transparent style={{alignSelf:'center',marginTop:5}}>
            <Text> ← Back to shopping</Text>
          </Button>
          
        </View>
      </Container>    
    )
  }
}

export default TestScreen