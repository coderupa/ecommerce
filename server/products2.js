var faker = require('faker')

function dummyImage(){
  var imageUrl=[]
  var category=['food','abstract','nature','technics','food','abstract','nature','technics','food','abstract','abstract']
  for (var i = 0; i < 10; i++) {
    for (var j = 1; j < 11; j++) {
      imageUrl.push(
        {
          url:"http://lorempixel.com/340/280/"+category[i]+'/'+j,
          category:category[i]
        }
      )
    }
  }
  return imageUrl
}

var imageUrl=dummyImage()

function generateProducts () {
  var products = []
  for (var id = 0; id < 100; id++) {
    products.push({
      "id":faker.random.number(),
      "productName": faker.commerce.productName(),
      "price": faker.commerce.price() * 10000,
      "productDescription": faker.lorem.paragraphs(),
      "image":imageUrl[id].url,
      "category": imageUrl[id].category 
    })
  }
  return { "products": products }
}
module.exports = generateProducts