import API from '../api'
 
import Reactotron from 'reactotron-react-native'
/**
 * Action Types
 */
const AUTH_REQUEST = 'user/auth_request'
const AUTH_SUCCESS = 'user/auth_success'
const AUTH_FAILURE = 'user/auth_failure'
const AUTH_RESET = 'user/auth_reset'

/**
 * Action Creator
 */
export const request=(email,password)=>(
  {
    type:AUTH_REQUEST
  }
)

export const success=(payload)=>(
  {
    type:AUTH_SUCCESS,
    payload

  }
)

export const failure=(errors)=>(
  {
    type:AUTH_FAILURE,
    errors
  }
)
 
export const reset= () =>({
  type:AUTH_RESET
})

export const getToken=()=>{
  return async(dispatch,getState) => {
    dispatch(request)
    try {
      let result = await API.getToken() 
      if (result.status==true) {
        dispatch(success(result))
      }else{
        dispatch(failure(result))
      }
    } catch (error) {
      dispatch(error)
    }
  }
}

export const validateToken=()=>{
  return async(dispatch,getState) => {
    // Reactotron.log(getState().auth.token)
    let token= getState().auth.token
    // token not exist, request it
    if (token ==''){
      await dispatch(getToken())
      if (getState().errors !==""){ 
        token = getState().auth.token
      }else{
        return 
      }
    }

    // validate token
    let appInfo = await API.appInfo(token)
    if (appInfo.status ==false && appInfo.errors[0].code=='401'){
      await dispatch(getToken())
      if (getState().errors !==""){ 
        token = getState().auth.token
      }else{
        return 
      }
    }
  }
}

let initialState={
  token:'',
  errors:''
}

/**
 * Reducer
 */
export default (state=initialState,action)=>{
  switch (action.type) {
    case AUTH_REQUEST:
      return {...state,isRequest:true}
    break;

    case AUTH_SUCCESS:
      return {
        ...state,...action.payload.data
      }
    break;

    case AUTH_FAILURE:
      return {
        ...state,errors:action.errors.errors
      }
    break;
  
    case AUTH_RESET:
      return initialState

    default:
    return state
      break;
  }
}
