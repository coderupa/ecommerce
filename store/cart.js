
// Action types
const UPDATE ='cart/update'
const REMOVE ='cart/remove'
const RESET ='cart/reset'

// Action Creators
export const update=(payload,replace=false) => (
  {
   type:UPDATE,
   payload,
   replace
 }
)

export const remove=(payload) =>(
 {
   type:REMOVE,
   payload
 }
)

export const reset=() =>(
 {
   type:RESET
 }
)

// Reducers
const initialState=[]
export default (state=initialState,action)=>{
  switch (action.type) {
    case UPDATE:
     let results=[...state]
     let findId
     findId=state.findIndex((obj => obj.id == action.payload.id ))
       if (findId >= 0){
         let currentQty=state[findId].Qty
         let newQty=action.replace==true ? action.payload.Qty : currentQty + action.payload.Qty
         results[findId]={...results[findId],Qty:newQty}
       }else{
         results=[...results,action.payload]
       }
     return results
     break;

    case REMOVE:
     return state.filter(product => product.id !== action.payload.id)
     break;
    
    case RESET:
       return initialState
     break;
       
     default:
     return state
     break;
  }
}