// import '../ReactotronConfig'
import { createStore,applyMiddleware,combineReducers,compose} from 'redux'
import { persistStore,persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk'
import cartReducer from './cart'
import productsReducer from './products'
import userReducer from './user'
import authReducer from './auth'
import { reducer as formReducer } from 'redux-form'
import {logger} from './myMiddleware'

// Reactotron Stuff
import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'


//  const reducers=combineReducers({
//       cart:cartReducer,
//       products:productsReducer,
//       user:userReducer,
//       auth:authReducer,
//       // editProfile:formReducer,
//       form:formReducer
//  })

const config = {
 key: 'root1',
 storage,
 whitelist:['user','cart','products']
}

const reducers=persistCombineReducers(config,{
// const reducers=combineReducers({
   cart:cartReducer,
   products:productsReducer,
   user:userReducer,
   auth:authReducer,
   editProfile:formReducer,
   form:formReducer
 })


// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const configureStore = () => {

//   const store = createStore(
//     reducers, 
//     composeEnhancers(applyMiddleware(...middleware),autoRehydrate())
//   );
//   persistStore(store,{storage:AsyncStorage,whitelist:['user']})
//   // persistStore(store,{storage:AsyncStorage,whitelist:['user']}).purge()
//   return store
// }
// export default configureStore

// const middleware = [logger,thunk] 
const middleware = [thunk] 
const configureStore = () => {
  // const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  // const store = Reactotron.createStore(reducers, composeEnhancers(
  const store = Reactotron.createStore(reducers, compose(
    applyMiddleware(...middleware)
  ))
  const persistor = persistStore(store)
  return { store, persistor }
  // return store
}

export default configureStore