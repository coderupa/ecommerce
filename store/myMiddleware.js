export const logger = store => next => action => {
  console.log('==============')
  console.log(action.type)
  console.log(typeof action)
  if(typeof action === 'function'){
    console.log(action.toString())
  }
  console.log('==============')

  // console.log('dispatching')
  let result = next(action)
  // console.log('next state')
  return result
}