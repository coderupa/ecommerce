import API from '../api'

// Action types
const FETCH_REQUEST ='product/fetch_request'
const FETCH_SUCCESS ='product/fetch_success'
const FETCH_FAILURE ='product/fetch_failure'
const SORT='products/sort'
const FILTER='products/filter'


// Action Creators
export const fetch=(status,result)=>{
  return async(dispatch,getState) => {

    let {data,count,page}=getState().products
    if(page > 0 &&  data.length >= count){
      // console.log('return this')
      return
    }

    dispatch({
      type:FETCH_REQUEST
    })

    let products = await API.readProducts(page+1)
    // const {data,page,pages,count} = products
    dispatch(
      {
        type:FETCH_SUCCESS,
        data:products.data,
        page:products.page,
        pages:products.pages,
        count:products.count
      }
    )

  }
}

export const sort=(payload)=>({
  type:SORT,
  payload
})

export const filter=(payload)=>({
  type:FILTER,
  payload
})

// export const filter=(payload)=>{
//   console.log(payload)
// }



 let initialProduct={
  data:[],
  page:0,
  pages:0,
  count:0,
  fetching:false,
  sort:'',
  filter:{min:0,max:0}
}

/**
 * filter:{
 *   price:{min:0,max:1000}
 * }
 */

export default (state=initialProduct,action)=>{
  //  console.log(state.products)
   switch (action.type) {
     case FETCH_REQUEST:
      return {...state,fetching:true}
     case FETCH_SUCCESS:
       return {...state,
        data:[...state.data,...action.data],
        count:action.count,
        page:action.page,
        pages:action.pages,
        fetching:false,
      }
      case SORT:
       return {...state,sort:action.payload}
       
      case FILTER:
      // console.log(action.payload)
      // return state
       return {...state,filter:action.payload}
      //  return {...state,filter:{...state.filter,...action.payload}}

     default:
       return state
       break;
   }
 }