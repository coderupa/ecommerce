import { createSelector } from 'reselect'

const getProducts = (state) => state.products

const queryProducts=(products)=>{
  // console.log('queryProducts')
  const {sort,filter,data}=products
  let result={...products}
  // let result=products
 
  if(sort){
    // console.log('sort ' + sort)
    let sortedProducts =_.orderBy(data, ['price'], [sort]);
    result={...result,data:sortedProducts}
  }

  if(filter.max>0){
    // console.log('filter ')
    let filteredProducts=_.filter(data,(item)=>(item.price >= filter.min && item.price <= filter.max))
    // result=_.filter(data,(item)=>(item.price >= filter.min && item.price <= filterPrice.max))
    result={...result,data:filteredProducts}
  }


  // console.log(sort)
    // this.setState({products:sorted})
  // return sortedProducts
  return result
 
}

export const getProductState = createSelector(
  [ getProducts ],
  (products) => queryProducts(products)
)

//queryProducts(state.products.filter,state.products.sort,state.products)