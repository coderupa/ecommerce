import API from '../api'

/**
 * Action Types
 */
const LOGIN_REQUEST = 'user/login_request'
const LOGIN_SUCCESS = 'user/login_success'
const LOGIN_FAILURE = 'user/login_failure'
const LOGIN_RESET = 'user/login_reset'

// const REGISTER_REQUEST = 'user/register_request'
// const REGISTER_SUCCESS = 'user/register_success'
// const REGISTER_FAILURE = 'user/register_failure'

/**
 * Action Creator
 */
export const request=(email,password)=>(
  {
    type:LOGIN_REQUEST
  }
)

export const success=(payload)=>(
  {
    type:LOGIN_SUCCESS,
    payload

  }
)

export const failure=(email,password)=>(
  {
    type:LOGIN_FAILURE
  }
)
 
export const reset= () =>({
  type:LOGIN_RESET
})


export const login = (email,password)=>{
  return async(dispatch,getState) => {
    dispatch(request(email,password))
    let result = await API.login(email,password)
    console.log(result)
    if (result.status==true) {
      dispatch(success(result))     
    }else{
      dispatch(failure(result))
    }
  }
}

// export const registerRequest=()=>({type:REGISTER_REQUEST})
// export const registerSuccess=(payload)=>({type:LOGIN_SUCCESS})
// export const failure=(email,password)=>({type:LOGIN_FAILURE}) 

// export const register =(email,name,password,image_url)=>{
//   return async(dispatch,getState) => {
//     // get token
//     dispatch(registerRequest)
//     let result = await API.register(email,name,password,image_url,token)  
//     if (result.status==true) {
//       dispatch(registerSuccess(result))     
//     }else{
//       dispatch(registerFailure(result))
//     } 
//   }
// }

/**
 * Reducer
 */
let initialState={
  isRequest:false,
  isFailure:false,
  isSuccess:false,
  data:"",
  token:''
}

export default (state=initialState,action)=>{
  switch (action.type) {
    case LOGIN_REQUEST:
      return {...state,isRequest:true}
    break;

    case LOGIN_SUCCESS:
      return {
        ...state,
        isRequest:false,
        data:action.payload.data.user,
        token:action.payload.data.token,
        isSuccess:true
      }
    break;

    case LOGIN_FAILURE:
      return {
        ...state,
        isRequest:false,
        isFailure:true,
        isSuccess:false
      }
    break;
  
    case LOGIN_RESET:
      return initialState

    default:
    return state
      break;
  }
}

